:::::::::::::::::::::::
:: INSTALL BATCH FILE  ::
:::::::::::::::::::::::
::
:: PATH TO CACHE DIRECTORY AND EXECUTABLES
set CACHEDIR=C:\InterSystems\Ensemble
set CACHEBIN=%CACHEDIR%\bin\cache
set CACHEINSTANCE=ENSEMBLE

:: Check build variables
set WORKSPACE=%CD%
set SRCDBDIR=%WORKSPACE%\db\
IF NOT a%1 == a SET NAMESPACE=%1
IF NOT DEFINED NAMESPACE SET NAMESPACE=%JOB_NAME%

:: PREPARE OUTPUT FILE
set OUTFILE=%CD%\outFile
del "%OUTFILE%"


:: NOW, PREPARE TO CALL CACHE
::
:: FIRST, LOAD BUILD CLASS TO USER NAMESPACE
echo set sc=$SYSTEM.OBJ.Load("%WORKSPACE%\school\Install.cls","ck") >inFile

:: IF UNSUCCESSFULL, DISPLAY ERROR
echo if sc'=1 do $SYSTEM.OBJ.DisplayError(sc) >>inFile

:: NOW, PERFORM INSTALL
echo if sc=1 set sc=##class(school.Install).Install("%NAMESPACE%","%SRCDBDIR%") >>inFile

:: IF UNSUCCESSFULL, DISPLAY ERROR
echo if sc'=1 do $SYSTEM.OBJ.DisplayError(sc) >>inFile

:: IF UNSUCCESSFULL, CREATE OUTPUT FLAG FILE
echo if sc'=1 set fileName="%OUTFILE%" o fileName:("NWS") u fileName do $SYSTEM.OBJ.DisplayError(sc) c fileName >>inFile

:: THAT'S IT
echo halt >>inFile

:: CALL CACHE

%CACHEBIN% -s %CACHEDIR%\mgr -U USER <inFile

:: TEST IF THERE WAS AN ERROR
IF EXIST "%OUTFILE%" EXIT 1
